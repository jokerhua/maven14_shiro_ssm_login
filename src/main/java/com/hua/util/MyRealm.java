package com.hua.util;

import com.hua.entity.SysPermission;
import com.hua.entity.SysUser;
import com.hua.service.SysRoleService;
import com.hua.service.SysUserService;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;

import javax.annotation.Resource;
import java.util.List;

/**
 * @BelongsPackage: com.hua.util
 * @author: JokerHua
 * @date: 2019-09-25 16:11
 * @Description: 文档注释
 */
public class MyRealm extends AuthorizingRealm {
    @Resource
    private SysUserService sysUserService;
    @Resource
    private SysRoleService sysRoleService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        System.out.println("授权");
        //SimpleAuthorizationInfo 封装用户的正确的角色和访问权限的字符信息
        SimpleAuthorizationInfo simpleAuthorizationInfo=new SimpleAuthorizationInfo();
        //1.得到认证通过的用户信息
        SysUser user=(SysUser)principalCollection.getPrimaryPrincipal();
        //2.根据用户名查询用户权限（角色和访问权限）->调用service
        List<String> rolenameList=sysRoleService.findroles(user.getId());
        //2.1添加角色列表
        simpleAuthorizationInfo.addRoles(rolenameList);
        //2.2查询得到访问权限，遍历添加
        for (String s : rolenameList) {
            List<SysPermission> menus = sysUserService.findMenus(s);
            for (SysPermission menu : menus) {
                simpleAuthorizationInfo.addStringPermission(menu.getpercode());
            }
        }
        for (String s : rolenameList) {
            List<SysPermission> permission = sysUserService.findPermission(s);
            for (SysPermission sysPermission : permission) {
                simpleAuthorizationInfo.addStringPermission(sysPermission.getpercode());
            }
        }
        return simpleAuthorizationInfo;
    }

    //认证(张三bcd 李四abc)
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //接受的是输入的用户名
        String username=(String) authenticationToken.getPrincipal();
        SysUser user = sysUserService.login(username);
        if(user==null){
            return null;
        }
        return new SimpleAuthenticationInfo(user,user.getPassWord(), ByteSource.Util.bytes(user.getSalt()),"myrealm");
    }
}
