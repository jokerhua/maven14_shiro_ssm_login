package com.hua.util;

import com.hua.entity.SysUser;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @BelongsPackage: com.hua.util
 * @author: JokerHua
 * @date: 2019-09-25 18:51
 * @Description: 文档注释
 */
public class MyFormFilter extends FormAuthenticationFilter {
    @Override
    protected boolean onLoginSuccess(AuthenticationToken token, Subject subject, ServletRequest request, ServletResponse response) throws Exception {
        //1.得到认证通过的主体
        SysUser user=(SysUser)subject.getPrincipal();
        //2.存储到session中（代替了原来的getuser）
        HttpServletRequest httpServletRequest= WebUtils.toHttp(request);
        HttpSession session=httpServletRequest.getSession();
        session.setAttribute("user1",user);
        return super.onLoginSuccess(token, subject, request, response);
    }
}
