package com.hua.util;

/**
 * @BelongsPackage: com.hua.util
 * @author: JokerHua
 * @date: 2019-09-26 15:48
 * @Description: 文档注释
 */
public class Message {
    private String str;

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }
}
