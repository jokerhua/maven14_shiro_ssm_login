package com.hua.controller;

import com.hua.entity.SysUser;
import com.hua.service.SysUserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @BelongsPackage: com.hua.controller
 * @author: JokerHua
 * @date: 2019-09-23 17:46
 * @Description: 文档注释
 */
@Controller
public class UserController {
    @Resource
    private SysUserService sysUserService;

    @RequestMapping("/gotoLogin")
    public String gotoLogin(HttpServletRequest request)throws Exception{
        //从FormAuthenticationFilter获得验证失败的错误信息
        String failure=(String) request.getAttribute("shiroLoginFailure");
        if (failure!=null){
            if(UnknownAccountException.class.getName().equals(failure)){
                throw new Exception("用户名错误");
            }else if(IncorrectCredentialsException.class.getName().equals(failure)){
                throw new Exception("密码错误");
            }else {
                throw new Exception("未知错误");
            }
        }
        return "login";
    }
    @RequestMapping("/getusers")
    public String getusers(HttpSession session){
        Subject subject= SecurityUtils.getSubject();
        SysUser user=(SysUser)subject.getPrincipal();
        session.setAttribute("user",user);
        System.out.println(session.getAttribute("user"));
        return "success";
    }
}
