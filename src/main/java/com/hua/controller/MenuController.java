package com.hua.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @BelongsPackage: com.hua.controller
 * @author: JokerHua
 * @date: 2019-09-23 21:40
 * @Description: 文档注释
 */
@Controller
public class MenuController {
    //  id ->11->张三
    @RequestMapping("/item/queryItem.action")
    @RequiresPermissions("item:select")
    @RequiresRoles("商品管理员")
    public String queryItem(){
        System.out.println("queryitem----");
        return "queryitem";
    }
    //id ->21->李四
    @RequestMapping("/user/query.action")
    @RequiresPermissions("user:query")
    @RequiresRoles("用户管理员")
    public String queryuser(){
        System.out.println("queryuser----");
        return "queryuser";
    }
}
