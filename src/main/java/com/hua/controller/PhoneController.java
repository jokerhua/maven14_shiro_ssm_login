package com.hua.controller;

import com.hua.cn.com.webxml.MobileCodeWS;
import com.hua.cn.com.webxml.MobileCodeWSSoap;
import com.hua.util.Message;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * @BelongsPackage: com.hua.controller
 * @author: JokerHua
 * @date: 2019-09-26 15:47
 * @Description: 文档注释
 */
@Controller
public class PhoneController {
    @Resource
    private MobileCodeWSSoap wsSoap;

    @RequestMapping("/getphonecity")
    @ResponseBody
    public Message getphonecity(String phone){
        /*MobileCodeWSSoap port = mobileCodeWS.getPort(MobileCodeWSSoap.class);
        String codeInfo=port.getMobileCodeInfo(phone,"");
        System.out.println(codeInfo);
        String[] split = codeInfo.split("：");
        Message message=new Message();
        message.setStr(split[1]);*/
        //更改为ssm+CXF
        //1.cxf依赖包开始    2.spring中<jaxws:client> serviceClass,address    3.以下代码
        //之前webservice已经添加了cn.com.webxml内文件，
        String codeInfo=wsSoap.getMobileCodeInfo(phone,"");
        String[] split = codeInfo.split("：");
        Message message=new Message();
        message.setStr(split[1]);
        return message;
    }
}
