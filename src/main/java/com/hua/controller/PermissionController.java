package com.hua.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @BelongsPackage: com.hua.controller
 * @author: JokerHua
 * @date: 2019-09-23 21:43
 * @Description: 文档注释
 */
@Controller
public class PermissionController {
    //id->12->张三
    @RequestMapping("/item/add.action")
    @RequiresPermissions("item:create")
    @RequiresRoles("商品管理员")
    public String additem(){
        System.out.println("additem----");
        return "additem";
    }
    //id->13->张三
    @RequestMapping("/item/editItem.action")
    @RequiresPermissions("item:update")
    @RequiresRoles("商品管理员")
    public String editItem(){
        System.out.println("editItem----");
        return "editItem";
    }
    //id->14
    @RequestMapping("/item/deleteitem.action")
    @RequiresPermissions("item:delete")
    public String deleteitem(){
        System.out.println("deleteitem----");
        return "deleteitem";
    }
    //id->15->张三
    @RequestMapping("/item/Itemselect.action")
    public String Itemselect(){
        System.out.println("Itemselect----");
        return "Itemselect";
    }

    //id->22->李四
    @RequestMapping("/user/adduser.action")
    @RequiresPermissions("item:query")
    @RequiresRoles("商品管理员")
    public String adduser(){
        System.out.println("adduser----");
        return "adduser";
    }
    //id->23
    @RequestMapping("/user/updateuser.action")
    @RequiresPermissions("user:update")
    public String updateuser(){
        System.out.println("updateuser----");
        return "updateuser";
    }

    //id->24
    @RequestMapping("/user/deleteuser.action")
    @RequiresPermissions("user:delete")
    public String deleteuser(){
        System.out.println("deleteuser----");
        return "deleteuser";
    }
}
