package com.hua.entity;

import java.util.List;

public class SysRole {
    private String id;
    private String name;
    private List<SysPermission> menulist;
    private List<SysPermission> permissionList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SysPermission> getMenulist() {
        return menulist;
    }

    public void setMenulist(List<SysPermission> menulist) {
        this.menulist = menulist;
    }

    public List<SysPermission> getPermissionList() {
        return permissionList;
    }

    public void setPermissionList(List<SysPermission> permissionList) {
        this.permissionList = permissionList;
    }


}
