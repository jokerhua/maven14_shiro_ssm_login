package com.hua.dao;

import com.hua.entity.SysPermission;
import com.hua.entity.SysUser;

import java.util.List;

/**
 * @BelongsPackage: com.hua.dao
 * @author: JokerHua
 * @date: 2019-09-23 17:57
 * @Description: 文档注释
 */
public interface SysUserMapper {
    SysUser login(String username);
    //根据用户id查询角色列表
    List<String> findrolesByuserid(String userid);
    //根据用户id查询菜单权限
    List<SysPermission> findmenus(String rolename);
    //根据用户id查询请求权限
    List<SysPermission> findPermission(String rolename);
}
