package com.hua.dao;

import java.util.List;

/**
 * @BelongsPackage: com.hua.dao
 * @author: JokerHua
 * @date: 2019-09-25 19:25
 * @Description: 文档注释
 */
public interface SysRoleMapper {
    List<String> findroles(String userid);
}
