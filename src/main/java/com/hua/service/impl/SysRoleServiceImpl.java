package com.hua.service.impl;

import com.hua.dao.SysRoleMapper;
import com.hua.service.SysRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @BelongsPackage: com.hua.service.impl
 * @author: JokerHua
 * @date: 2019-09-25 19:19
 * @Description: 文档注释
 */
@Service
public class SysRoleServiceImpl implements SysRoleService {
    @Resource
    private SysRoleMapper sysRoleMapper;
    @Override
    public List<String> findroles(String userid) {
        return sysRoleMapper.findroles(userid);
    }
}
