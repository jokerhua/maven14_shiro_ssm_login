package com.hua.service.impl;

import com.hua.dao.SysUserMapper;
import com.hua.entity.SysPermission;
import com.hua.entity.SysRole;
import com.hua.entity.SysUser;
import com.hua.service.SysUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @BelongsPackage: com.hua.service.impl
 * @author: JokerHua
 * @date: 2019-09-23 17:43
 * @Description: 文档注释
 */
@Service
public class SysUserServiceImpl implements SysUserService {
    @Resource
    private SysUserMapper sysUserMapper;

    @Override
    public SysUser login(String username) {
        SysUser sysUser=sysUserMapper.login(username);
        return sysUser;
    }

    @Override
    public List<SysPermission> findMenus(String rolename) {
        return sysUserMapper.findmenus(rolename);
    }

    @Override
    public List<SysPermission> findPermission(String rolename) {
        return sysUserMapper.findPermission(rolename);
    }
}
