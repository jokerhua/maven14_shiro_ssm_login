package com.hua.service;

import java.util.List;

public interface SysRoleService {
    //通过用户id查询角色列表
    public List<String> findroles(String userid);
}
