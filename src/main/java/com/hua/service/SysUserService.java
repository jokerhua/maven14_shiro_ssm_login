package com.hua.service;

import com.hua.entity.SysPermission;
import com.hua.entity.SysUser;

import java.util.List;

public interface SysUserService {
    SysUser login(String username);

    List<SysPermission> findMenus(String rolename);

    List<SysPermission> findPermission(String rolename);
}
